# CMS CSC SWPC monitoring

This repo contains the script to run the monitoring tools of single wire chambers for the CMS-CSC gas system.

# Setup and Installation

## Evironment
If the PC is new a python distribution with the proper environment is required. To do so you can follow these steps:

1. Install a python distribution via anaconda from here: https://www.anaconda.com/products/individual
2. Install git from here: https://git-scm.com/downloads
3. (Optional but useful) install Visual Studio code editor from CMF or from the official website: https://code.visualstudio.com/

## Program
To get the actual program clone the repo:
```prompt
$ cd <folder>
$ git clone <url>
```
Open an anaconda prompt and create an environment from file:
```
$ conda create --name <env> --file spec-file.txt
```
