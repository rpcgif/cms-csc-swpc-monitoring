import functions as f
from environs import Env
from influxdb import DataFrameClient, InfluxDBClient
import urllib3
import datetime
from pathlib import Path

urllib3.disable_warnings()
env = Env()
env.read_env()

def test_influxdb_query():
    query = 'SELECT "Float_value" FROM "dip" WHERE ("sub" = \'dip/CMS/GCS/CMSAUX/Analysis/Chain3/Source154\' AND "dip_tag" = \'O2(L3Sr154O2AS)\') ORDER BY time DESC LIMIT 1'
    HOST = env.str("INFLUXDB_HOST")
    PORT = env.int("INFLUXDB_PORT")
    USERNAME = env.str("INFLUXDB_USERNAME")
    PASSWORD = env.str("INFLUXDB_PASSWORD")
    DATABASE = env.str("INFLUXDB_DATABASE")
    SSL = env.bool("INFLUXDB_SSL")
    VERIFYSSL = env.bool("INFLUXDB_VERIFYSSL")
    """
    res = res = f.run_influxdb_query(
        query,
        HOST,
        PORT,
        USERNAME,
        PASSWORD,
        DATABASE,
        SSL,
        VERIFYSSL,
    )
    """
    print(HOST, PORT, USERNAME, PASSWORD, DATABASE, SSL, VERIFYSSL)
    client = DataFrameClient(HOST, PORT, USERNAME, PASSWORD, DATABASE, SSL, VERIFYSSL)
    res = client.query(query)
    return res


def test_retrieve_dip_data():
    subscription = "dip/CMS/GCS/CMSAUX/Analysis/Chain3/Source155"
    tag = "O2(L3Sr155O2AS)"
    res = f.retrieve_dip_data(env, subscription, tag, n=2)
    return res


def test_retrieve_o2_values():
    line = 154
    n = 3
    return f.retrieve_o2_values(env, line, n)


def test_get_picolog_values():
    values = f.get_picolog_values()
    return values

def test_write_to_db():
    HOST = env.str("INFLUXDB_HOST")
    PORT = env.int("INFLUXDB_PORT")
    USERNAME = env.str("INFLUXDB_USERNAME")
    PASSWORD = env.str("INFLUXDB_PASSWORD")
    DATABASE = "test"
    SSL = env.bool("INFLUXDB_SSL")
    VERIFYSSL = env.bool("INFLUXDB_VERIFYSSL")
    measurement = "test"
    tags = {
        "setup": "csc-swpc",
        "script": "python",
        "device": "virtual"
    }
    fields = {
        "Float_value": 1
    }
    res = f.write_to_db(HOST, PORT, USERNAME, PASSWORD, DATABASE, measurement, tags, fields, time=None, ssl=SSL, verify_ssl=VERIFYSSL)
    print('Result of writing points: ', res)
    time = "2021-03-18T00:00:01.000000Z"
    res = f.write_to_db(HOST, PORT, USERNAME, PASSWORD, DATABASE, measurement, tags, fields, time=time, ssl=SSL, verify_ssl=VERIFYSSL)
    print('Result of writing points custom time: ', res)


def test_run_wavedump_acquisition():
    basepath = Path('.').absolute()
    wavedump_basepath = basepath / 'wavedump-custom'
    wavedump_path = wavedump_basepath / 'WaveDump.exe'
    wavedump_config_path = basepath / 'config' / 'WaveDumpConfigDET1.txt'
    output_folder = r"C:\Users\rpcgif\AppData\Local\Temp"
    n_events = 10
    f.run_wavedump_acquisition(wavedump_path, wavedump_config_path, output_folder, n_events, auto_trigger=True)

if __name__ == "__main__":
    test_run_wavedump_acquisition()
