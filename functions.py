from picosdk.picohrdl import picohrdl as hrdl
from picosdk.functions import assert_pico2000_ok
from influxdb import DataFrameClient, InfluxDBClient
from environs import Env
import ctypes
import os
from pathlib import Path
import subprocess


env = Env()
env.read_env()


def run_influxdb_query(
    query, host, port, username, password, database, ssl=True, verify_ssl=False
):
    """Run a query on infxludb and return the result"""
    client = DataFrameClient(host, port, username, password, database, ssl, verify_ssl)
    results = client.query(query)
    return results


def retrieve_dip_data(env: Env, subscription: str, tag: str, n=1):
    """Retrieve last n measures from a dip subscription"""
    HOST = env.str("INFLUXDB_HOST")
    PORT = env.int("INFLUXDB_PORT")
    USERNAME = env.str("INFLUXDB_USERNAME")
    PASSWORD = env.str("INFLUXDB_PASSWORD")
    DATABASE = env.str("INFLUXDB_DATABASE")
    SSL = env.bool("INFLUXDB_SSL")
    VERIFYSSL = env.bool("INFLUXDB_VERIFYSSL")
    query = f'SELECT "Float_value" FROM "dip" WHERE ("sub" = \'{subscription}\' AND "dip_tag" = \'{tag}\') ORDER BY time DESC LIMIT {n}'
    res = run_influxdb_query(
        query, HOST, PORT, USERNAME, PASSWORD, DATABASE, SSL, VERIFYSSL
    )
    return res["dip"]


def write_to_db(
    host: str,
    port: int,
    username: str,
    password: str,
    database: str,
    measurement: str,
    tags: "dict",
    fields: "dict",
    time: str = None,
    ssl: bool = True,
    verify_ssl: bool = False,
):
    client = InfluxDBClient(host, port, username, password, database, ssl, verify_ssl)
    timestamp = {"time": time} if time else {}
    body = [{"measurement": measurement, "tags": tags, "fields": fields, **timestamp}]
    return client.write_points(body)


def retrieve_o2_values(env: Env, line: int, n=1):
    """Retrieve last n value from an O2 line dip subscription (154, 155)"""
    subscription = f"dip/CMS/GCS/CMSAUX/Analysis/Chain3/Source{line}"
    tag = f"O2(L3Sr{line}O2AS)"
    res = retrieve_dip_data(env, subscription, tag, n)
    return res


def get_default_picolog_configs():
    """Return a dict with the default configs of a picolog reader"""
    picolog_configs = {
        "range": hrdl.HRDL_VOLTAGERANGE["HRDL_2500_MV"],
        "channels": range(1, 17),
        "conversion_time": hrdl.HRDL_CONVERSIONTIME["HRDL_100MS"],
        "range_map": {0: 2500},
    }
    return picolog_configs


def open_picolog(picolog_configs):
    """Open picolog given a dict of configs. Return the handle """
    picolog_handle = ctypes.c_int16()
    picolog_handle = hrdl.HRDLOpenUnit()
    picolog_configs["handle"] = picolog_handle
    assert_pico2000_ok(picolog_configs["handle"])
    picolog_configs["mains_rejection"] = hrdl.HRDLSetMains(picolog_configs["handle"], 0)
    assert_pico2000_ok(picolog_configs["mains_rejection"])
    return picolog_handle


def close_picolog(handle):
    """Close picolog"""
    return hrdl.HRDLCloseUnit(handle)


def get_adc_counts_picolog(picolog_configs):
    """Return a dict with adc count for each channel"""
    min_adc = ctypes.c_int32()
    max_adc = ctypes.c_int32()
    adc_channels = {}
    for channel in picolog_configs["channels"]:
        channel_num = ctypes.c_int16(channel)
        hrdl.HRDLGetMinMaxAdcCounts(
            picolog_configs["handle"],
            ctypes.byref(min_adc),
            ctypes.byref(max_adc),
            channel_num,
        )
        adc_channels[channel] = max_adc.value

    return adc_channels


def get_picolog_values(
    picolog_configs=None, adc_counts=None, conversion_functions=None
):
    """Return the values from the channel of a picolog.

    Parameters
    ----------
    picolog_configs: optional, dict
        a dict containing the necessary information to open the picolo
    adc_counts: optional, dict of ints
        a dict with key representing the channel and value the max adc
    conversion_functions: optional, dict of functions
        a dict with key representing the channel and value a function to
        transform mV to physical unit. By default an identity function
        is applied if nothing is

    Returns
    -------
    dict of floats:
        a dict where keys represent the channel and values the mV or physical
        values
    """
    picolog_configs = picolog_configs or get_default_picolog_configs()
    handle = picolog_configs.get("handle", open_picolog(picolog_configs))
    try:
        adc_counts = adc_counts or get_adc_counts_picolog(picolog_configs)
        overflow = ctypes.c_int16(0)
        conversion_time_code = picolog_configs["conversion_time"]
        max_range_code = picolog_configs["range"]
        max_range_mv = picolog_configs["range_map"][max_range_code]
        single_ended = 1
        values = {}
        conversion_functions = conversion_functions or {}
        raw_adc = ctypes.c_int16()
        for channel in picolog_configs["channels"]:
            max_adc = adc_counts[channel]
            raw_adc = ctypes.c_int32()
            channel_num = ctypes.c_int16(channel)
            hrdl.HRDLGetSingleValue(
                handle,
                channel_num,
                picolog_configs["range"],
                conversion_time_code,
                single_ended,
                ctypes.byref(overflow),
                ctypes.byref(raw_adc),
            )
            mv_value = raw_adc.value / max_adc * max_range_mv
            conversion_function = conversion_functions.get(channel, lambda x: x)
            values[channel] = conversion_function(mv_value)
    except:
        close_picolog(handle)
    return values


def run_wavedump_acquisition(wavedump_path: str, wavedump_config_path: str, output_folder: str, n_events: int, auto_trigger=False):
    """Run a wavedump acquisition and write data to file.
    
    Parameters
    ----------
    wavedump_path: str
        path to the wavedump executable
    wavedump_config_path: str
        path to the wavedump config .txt file to use
    output_folder: str
        path to the output folder to write. If not existing the folder
        will be created
    n_events: int
        the number of events to acquire
    auto_trigger: bool default False
        enable or disable auto trigger mode on digitizer
    
    """
    wavedump_path = Path(wavedump_path)
    assert wavedump_path.exists()
    wavedump_config_path = Path(wavedump_config_path)
    assert wavedump_config_path.exists()
    output_folder = Path(output_folder)
    output_folder.mkdir(exist_ok=True, parents=True)
    res = subprocess.run([wavedump_path, wavedump_config_path, output_folder, str(n_events), str(int(auto_trigger))], check=True)
    return res